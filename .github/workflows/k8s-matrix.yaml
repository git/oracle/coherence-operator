# Copyright 2020, 2022, Oracle Corporation and/or its affiliates.  All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at
# http://oss.oracle.com/licenses/upl.

# ---------------------------------------------------------------------------
# Coherence Operator GitHub Actions K8s Certification build.
# ---------------------------------------------------------------------------
name: K8s Certification

on:
  workflow_dispatch:
  push:
    branches-ignore:
    - gh-pages
    - 1.0.0
    - 2.x
    - 3.x
  pull_request:
    types:
      - opened
      - synchronize
      - committed
    branches-ignore:
    - gh-pages
    - 1.0.0
    - 2.x
    - 3.x

env:
  MAVEN_USER: ${{ secrets.MAVEN_USER }}
  MAVEN_PASSWORD: ${{ secrets.MAVEN_PASSWORD }}
  LOAD_KIND: true

jobs:
  build:
    runs-on: ubuntu-latest

    strategy:
      fail-fast: false
      matrix:
        k8s:
          - kindest/node:v1.25.0@sha256:428aaa17ec82ccde0131cb2d1ca6547d13cf5fdabcc0bbecf749baa935387cbf
          - kindest/node:v1.24.4@sha256:adfaebada924a26c2c9308edd53c6e33b3d4e453782c0063dc0028bdebaddf98
          - kindest/node:v1.23.10@sha256:f047448af6a656fae7bc909e2fab360c18c487ef3edc93f06d78cdfd864b2d12
          - kindest/node:v1.22.13@sha256:4904eda4d6e64b402169797805b8ec01f50133960ad6c19af45173a27eadf959
          - kindest/node:v1.21.14@sha256:f9b4d3d1112f24a7254d2ee296f177f628f9b4c1b32f0006567af11b91c1f301
          - kindest/node:v1.20.15@sha256:d67de8f84143adebe80a07672f370365ec7d23f93dc86866f0e29fa29ce026fe
          - kindest/node:v1.19.16@sha256:707469aac7e6805e52c3bde2a8a8050ce2b15decff60db6c5077ba9975d28b98
          - kindest/node:v1.18.20@sha256:61c9e1698c1cb19c3b1d8151a9135b379657aee23c59bde4a8d87923fcb43a91

    steps:
    - uses: actions/checkout@v2
      with:
        fetch-depth: 0

#   This step will free up disc space on the runner by removing
#   lots of things that we do not need.
    - name: disc
      shell: bash
      run: |
        echo "Listing 100 largest packages"
        dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -n | tail -n 100
        df -h
        echo "Removing large packages"
        sudo apt-get remove -y '^dotnet-.*'
        sudo apt-get remove -y '^llvm-.*'
        sudo apt-get remove -y 'monodoc-http'
        sudo apt-get remove -y 'php.*'
        sudo apt-get remove -y azure-cli google-cloud-sdk hhvm google-chrome-stable firefox powershell mono-devel
        sudo apt-get autoremove -y
        sudo apt-get clean
        df -h
        echo "Removing large directories"
        rm -rf /usr/share/dotnet/
        df -h

    - name: Set up JDK
      uses: actions/setup-java@v3
      with:
        distribution: 'zulu'
        java-version: '11'

    - name: Cache Go Modules
      uses: actions/cache@v1
      with:
        path: ~/go/pkg/mod
        key: ${{ runner.os }}-go-mods-${{ hashFiles('**/go.sum') }}
        restore-keys: |
          ${{ runner.os }}-go-mods-

    - name: Cache Maven packages
      uses: actions/cache@v1
      with:
        path: ~/.m2
        key: ${{ runner.os }}-m2-${{ hashFiles('**/pom.xml') }}
        restore-keys: ${{ runner.os }}-m2

    - name: Set up Go
      uses: actions/setup-go@v2
      with:
        go-version: 1.17.x

    - name: Edit DNS Resolve
      shell: bash
      run: |
        sudo chown -R runner:runner /run/systemd/resolve/stub-resolv.conf
        sudo echo nameserver 8.8.8.8 > /run/systemd/resolve/stub-resolv.conf

    - name: Start KinD Cluster
      shell: bash
      run: |
        make kind KIND_IMAGE=${{ matrix.k8s }}
        kubectl version
        kubectl get nodes
        docker pull gcr.io/distroless/java
        docker pull gcr.io/distroless/java11-debian11
        docker pull gcr.io/distroless/java17-debian11

    - name: Certification Tests
      shell: bash
      run: ./hack/k8s-certification.sh

    - uses: actions/upload-artifact@v1
      if: failure()
      with:
        name: test-output
        path: build/_output/test-logs
