# Copyright 2019, 2022, Oracle Corporation and/or its affiliates.  All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at
# http://oss.oracle.com/licenses/upl.

# ---------------------------------------------------------------------------
# Coherence Operator GitHub Actions CI build.
# ---------------------------------------------------------------------------
name: Prometheus Tests

on:
  workflow_dispatch:
  push:
    branches-ignore:
    - gh-pages
    - 1.0.0
    - 2.x
    - 3.x
  pull_request:
    types:
      - opened
      - synchronize
      - committed
    branches-ignore:
    - gh-pages
    - 1.0.0
    - 2.x
    - 3.x

jobs:
  build:
    runs-on: ubuntu-latest

#   Checkout the source, we need a depth of zero to fetch all the history otherwise
#   the copyright check cannot work out the date of the files from Git.
    steps:
    - uses: actions/checkout@v2
      with:
        fetch-depth: 0

#   This step will free up disc space on the runner by removing
#   lots of things that we do not need.
    - name: disc
      shell: bash
      run: |
        echo "Listing 100 largest packages"
        dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -n | tail -n 100
        df -h
        echo "Removing large packages"
        sudo apt-get remove -y '^dotnet-.*'
        sudo apt-get remove -y '^llvm-.*'
        sudo apt-get remove -y 'monodoc-http'
        sudo apt-get remove -y 'php.*'
        sudo apt-get remove -y azure-cli google-cloud-sdk hhvm google-chrome-stable firefox powershell mono-devel
        sudo apt-get autoremove -y
        sudo apt-get clean
        df -h
        echo "Removing large directories"
        rm -rf /usr/share/dotnet/
        df -h

    - name: Set up JDK
      uses: actions/setup-java@v3
      with:
        distribution: 'zulu'
        java-version: '11'

    - name: Cache Go Modules
      uses: actions/cache@v1
      with:
        path: ~/go/pkg/mod
        key: ${{ runner.os }}-go-mods-${{ hashFiles('**/go.sum') }}
        restore-keys: |
          ${{ runner.os }}-go-mods-

    - name: Cache Maven packages
      uses: actions/cache@v1
      with:
        path: ~/.m2
        key: ${{ runner.os }}-m2-${{ hashFiles('**/pom.xml') }}
        restore-keys: ${{ runner.os }}-m2

    - name: Set up Go
      uses: actions/setup-go@v2
      with:
        go-version: 1.17.x

    - name: Edit DNS Resolve
      shell: bash
      run: |
        sudo chown -R runner:runner /run/systemd/resolve/stub-resolv.conf
        sudo echo nameserver 8.8.8.8 > /run/systemd/resolve/stub-resolv.conf

    - name: Start KinD Cluster
      shell: bash
      run: |
        make kind
        kubectl version
        kubectl get nodes
        docker pull gcr.io/distroless/java
        docker pull gcr.io/distroless/java11-debian11
        docker pull gcr.io/distroless/java17-debian11

    - name: E2E Prometheus Tests
      shell: bash
      timeout-minutes: 45
      run: |
        make all
        make kind-load
        make e2e-prometheus-test

    - uses: actions/upload-artifact@v1
      if: failure()
      with:
        name: test-output
        path: build/_output/test-logs
